<?php

declare(ticks = 1);
require_once("app.php");

// Create child process
$child_pid = pcntl_fork();

if ($child_pid) {
    // Exit parent process and release console
    exit();
}

// Set current process as parent
posix_setsid();

$daemon = new Daemon();

// Check double launch
if(!$daemon->isDaemonActive()) {

    redirect_StreamIO();

    // Create scheduled task list, parsing from file: ScheduledTask.list
    $ScheduledTaskList = (new ScheduledTask\ScheduledTaskFactory)->make('./ScheduledTask.list');

    $daemon->setScheduledTaskList($ScheduledTaskList);
    $daemon->run();

} else {
    echo 'Daemon already work'.PHP_EOL;
}
