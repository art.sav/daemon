<?php

namespace Components;

class XorChiper
{
    public static function make(string $text, string $key): string
    {
        $outText = '';
        for($i=0; $i<strlen($text); )
        {
            for($j=0; ($j<strlen($key) && $i<strlen($text)); $j++,$i++)
            {
                $outText .= $text{$i} ^ $key{$j};
            }
        }
        return $outText;
    }
}
