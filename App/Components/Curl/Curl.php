<?php

namespace Components\Curl;

/**
 * This class implements a wrapper for standard cUrl
 */
class Curl
{
    /** @var string|false contains name file cookies or false */
    private $cookies;

    /** @var cURL contains cURL handle */
    private $curl;

    public function __construct(CurlConfig $curlConfig)
    {
        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_USERAGENT, $curlConfig->getRandomUserAget());
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $curlConfig->timeOut);
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, $curlConfig->timeOut);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_HEADER, 0);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($this->curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $curlConfig->headers);

        if ($curlConfig->use_cookies) {
            $this->cookies = $curlConfig->use_cookies;
            $this->setCookie();
        }
    }

    public function __destruct()
    {
        curl_close($this->curl);
    }

    public function get($url)
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
        $output = curl_exec($this->curl);
        return $output;
    }

    public function delete($url)
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        $output = curl_exec($this->curl);
        return $output;
    }

    public function post($url, $param)
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, ((is_string($param))?$param:http_build_query($param)));
        $output = curl_exec($this->curl);
        return $output;
    }

    public function getReturnCode($param = null)
    {
        $info = curl_getinfo($this->curl);
        if ($param != null) {
            return $info[$param];
        }
        return $info;
    }

    public function getError()
    {
        return curl_error($this->curl);
    }

    private function setCookie()
    {
        $path = md5($this->cookies);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, sprintf('%s/%s.cookies.txt',sys_get_temp_dir(),$path));
        curl_setopt($this->curl, CURLOPT_COOKIEFILE, sprintf('%s/%s.cookies.txt',sys_get_temp_dir(),$path));
        curl_setopt($this->curl, CURLOPT_COOKIESESSION, false);
    }

};
