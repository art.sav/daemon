<?php

use Exceptions\{WorkerException, DaemonException};
use Workers\{IWorker, TestWorker};
use ScheduledTask\ScheduledTaskList;


class Daemon
{
    /** @var ScheduledTaskList contains object ScheduledTaskList */
    protected $ScheduledTaskList;

    /** @var boolean this daemon working until this var equivalent true  */
    protected $workDaemon = true;

    /** @var array contains list pid running workers */
    protected $childWorkers = [];

    /** @var array contains path pid file for daemon */
    protected $pidFilePath;

    public function __construct($pidFilePath = null)
    {
        // set handler for signals
        pcntl_signal(SIGTERM, [$this, "childSignalHandler"]);
        pcntl_signal(SIGCHLD, [$this, "childSignalHandler"]);

        if($pidFilePath == null) {
            $pidFilePath = sprintf('%s/daemon-php.pid',sys_get_temp_dir());
        }
        $this->pidFilePath = $pidFilePath;
    }

    protected function addWorker($pid)
    {
        $this->childWorkers[$pid] = true;
        return true;
    }

    public function setScheduledTaskList(ScheduledTaskList $ScheduledTaskList)
    {
        $this->scheduledTaskList = $ScheduledTaskList;
        return $this;
    }

    protected function checkWorker($pid)
    {
        return isset($this->currentJobs[$pid]) && $this->currentJobs[$pid] == true;
    }

    protected function removeWorker($pid)
    {
        unset($this->childWorkers[$pid]);
        return true;
    }

    public function run()
    {
        if(empty($this->scheduledTaskList)) {
            throw new \DaemonException("Not set scheduled task list", 1);
        }

        if(count($this->scheduledTaskList->getAllScheduledTask()) == 0) {
            throw new \DaemonException("Empty scheduled task list", 1);
        }

        $this->createPid();

        while( $this->workDaemon ) {

            // receive tasks from ScheduledTaskList for run now
            $scheduledTaskList = $this->scheduledTaskList->getScheduledTaskForRunNow();

            foreach ($scheduledTaskList as $key => $scheduledTask) {
                $className = $scheduledTask->getWorkerClass();
                echo sprintf('pid: %s, Class: %s, date: %s',getmypid(), $className, date("Y-m-d G:i")).PHP_EOL;
                $this->startWorker(new $className());
            }

            sleep(60);
        }

    }

    /**
     * Starts up worker and add it pid in common list
     *
     * @param IWorker instance worker for start
     * @return boolean;
     */
    public function startWorker(IWorker $worker)
    {
        // savePid parent process
        $thisPid = getmypid();

        // create child process
        $pid = pcntl_fork();
        if ($pid == -1) {   // fail create child process

            throw new WorkerException("Fail start worker", 1);

        } else if ($pid > 0) { // execute only parent process

            $this->addWorker($pid);
            pcntl_waitpid($pid, $status); // blocking zombie

        } else if ($pid === 0) { // execute only child process

            try {
                // run worker
                $worker->setParentPid($thisPid)->run();
            } catch(WorkerException $e) {
                $this->failDaemon($e->__toString());
            }
            exit();

        }

        return true;
    }

    /**
     * Handler signals
     *
     * @param int $signo signal
     * @param int pid process
     * @param int status
     * @return boolean;
     */
    public function childSignalHandler($signo, $pid = null, $status = null) {

        switch($signo) {
            case SIGTERM:
                // signal stopping daemon

                $this->workDaemon = false;
                break;
            case SIGCHLD:
                // signal update child process

                if (!$pid) {
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }

                // check pid ending process
                while ($pid > 0) {
                    if ($pid && $this->checkWorker($pid)) {
                        // remove child process
                        $this->removeWorker($pid);
                    }
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                break;
            default:
                // other signals
            break;
        }

    }

    /**
     * Checks if the daemon is already running
     *
     * @return boolean;
     */
    public function isDaemonActive()
    {
        if( file_exists($this->pidFilePath) && is_file($this->pidFilePath) ) {

            $pid = file_get_contents($this->pidFilePath);

            // if process is alive
            if(posix_kill($pid, 0)) {
                return true;
            } else {
                // remove pid file
                if(!unlink($this->pidFilePath)) {
                    throw new \DaemonException("old PID file not delete", 1);
                }
            }
        }
        return false;
    }

    private function createPid()
    {
        file_put_contents($this->pidFilePath, getmypid());
    }

    protected function endDaemon()
    {
        $this->childSignalHandler(SIGTERM);
    }

    /**
     * Emergency ending work this daemon
     * Send email and stop daemon
     *
     * @param string error message
     * @return null;
     */
    protected function failDaemon(string $message)
    {
        \Components\Mailer::sendEmail('PHP Daemon exception', $message);
        $this->endDaemon();
    }

}
