<?php

namespace Workers;

/**
 * This abstract class Worker, realizing common functions for all Workers: getParentPid & endWorker
 */
abstract class BaseWorker implements IWorker
{
    /** @var int|null contains parent pid */
    protected $parentPid;

    public function setParentPid(int $pid): IWorker
    {
        $this->parentPid = $pid;
        return $this;
    }

    abstract public function run();

    public function endWorker()
    {
        // Send update signal for parent process by pid
        posix_kill($this->parentPid, SIGCHLD);
        pcntl_signal_dispatch();
    }
}
