<?php

namespace Workers;

use \Components\XorChiper;
use \Components\Curl\{Curl, CurlConfig};
use \Exceptions\WorkerException;

class SynergyWorker extends BaseWorker
{

    public function run()
    {
        $Curl = new Curl(new CurlConfig());

        $responseJson = $Curl->post('https://syn.su/testwork.php',['method' => 'get']);

        if(($response = $this->checkJsonResponse($responseJson)) == false) {
            return false;
        }

        $messageXor = XorChiper::make($response->response->message, $response->response->key);
        $messageBase = base64_encode($messageXor);

        $responseAfterSendChiperJson = $Curl->post('https://syn.su/testwork.php',['method' => 'update', 'message' => $messageBase ]);

        if(($responseAfterSendChiper = $this->checkJsonResponse($responseAfterSendChiperJson)) == false) {
            return false;
        }

        if(isset($responseAfterSendChiper->errorCode) && $responseAfterSendChiper->errorCode != null) {
            $this->fail($responseAfterSendChiper->errorMessage);
        }
        
        echo 'good: '.$responseAfterSendChiper->response.PHP_EOL;
        $this->endWorker();
        return true;
    }

    private function checkJsonResponse(string $responseJson)
    {
        if(empty($responseJson)) {
            $this->fail('empty response from server');
            return false;
        }


        $response = json_decode($responseJson);

        if(!is_object($response)) {
            $this->fail('bad json response from server');
            return false;
        }

        return $response;
    }

    public function fail($message = '')
    {
        throw new WorkerException("SynergyWorker: ".$message, 1);
    }
}
