<?php

namespace Workers;

/**
 * Interface worker
 */
interface IWorker
{
    /**
     * Execute worker
     *
     * @return boolean;
     */
    public function run();

    /**
     * Set pid parent process
     *
     * @param int $pid
     * @return IWorker $this;
     */
    public function setParentPid(int $pid): IWorker;


    /**
     * Ending current worker
     *
     * @return null;
     */
    public function endWorker();

}
