<?php

namespace ScheduledTask;

class ScheduledTaskList
{
    /** @var array contains list all ScheduledTask */
    private $list = [];

    /**
     * Add ScheduledTask for common list
     *
     * @param ScheduledTask
     * @return null;
     */
    public function addScheduledTask(ScheduledTask $ScheduledTask)
    {
        $this->list[] = $ScheduledTask;
    }

    public function getScheduledTaskForRunNow()
    {
        return $this->getScheduledTaskForRunByTime(time());
    }

    /**
     * Search functions to be execute at the given timestamp
     *
     * @param int timestamp
     * @return ScheduledTask[];
     */
    public function getScheduledTaskForRunByTime($timestamp)
    {
        $tasks = [];

        foreach ($this->list as $key => $ScheduledTask) {
            if($ScheduledTask->isRunByTime($timestamp)) {
                $tasks[] = $ScheduledTask;
            }
        }

        return $tasks;
    }

    public function getAllScheduledTask()
    {
        return $this->list;
    }

}
