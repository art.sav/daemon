<?php

namespace ScheduledTask;

use Exceptions\ScheduledTaskException;

/**
 * This factory for create ScheduledTaskList with help parsing ScheduledTask.list
 */
class ScheduledTaskFactory
{

    /**
     * Make ScheduledTaskList with help parsing ScheduledTask.list
     *
     * @param string path to file ScheduledTask.list
     * @return ScheduledTaskList;
     */
    public function make($tableFilePath)
    {
        if(($tableFilePath = \FileSystem::checkFile($tableFilePath)) == false) {
            throw new ScheduledTaskException("File table not found", 1);
        }

        $dataList = $this->parse($tableFilePath);

        $ScheduledTaskList = new ScheduledTaskList();

        foreach ($dataList as $key => $data) {

            $ScheduledTask = new ScheduledTask();
            $ScheduledTask->setMinute($data['minute']);
            $ScheduledTask->setHour($data['hour']);
            $ScheduledTask->setDay($data['day']);
            $ScheduledTask->setMonth($data['month']);
            $ScheduledTask->setDayWeek($data['dayWeek']);
            $ScheduledTask->setWorkerClass($data['workerClass']);

            $ScheduledTaskList->addScheduledTask($ScheduledTask);
        }

        return $ScheduledTaskList;
    }


    /**
     * Parsing ScheduledTask.list
     *
     * @param string path to file ScheduledTask.list
     * @return array[];
     */
    private function parse($tableFilePath)
    {

        $tableFileContent = file_get_contents($tableFilePath);

        // remove comments
        $tableFileContent = trim(preg_replace('/^\#.*$/mi','',$tableFileContent));

        // split by line by line
        $tableFileLine = explode(PHP_EOL, $tableFileContent);

        $res = [];

        foreach ($tableFileLine as $key => $line) {
            if(($tmpData = $this->parseLine($line)) != false) {
                $res[] = $tmpData;
            }
        }

        return $res;
    }


    /**
     * Parsing line kind of: * * * * * BaseWorker
     *
     * @param string line task;
     * @return array[minute,hour,day,month,dayWeek,workerClass];
     */
    private function parseLine($line)
    {
        $data = [];
        list($data['minute'],
             $data['hour'],
             $data['day'],
             $data['month'],
             $data['dayWeek'],
             $data['workerClass']) = sscanf($line, '%s %s %s %s %s %s'); //preg_split('/(\s|\t)/', $line, 6);

        $data['workerClass'] = trim($data['workerClass']);

        if($data['minute'] != '*' && !($data['minute']>=0 && $data['minute']<=59)) {
            return false;
        }

        if($data['hour'] != '*' && !($data['hour']>=0 && $data['hour']<=23)) {
            return false;
        }

        if($data['day'] != '*' && !($data['day']>=1 && $data['day']<=31)) {
            return false;
        }

        if($data['month'] != '*' && !($data['month']>=1 && $data['month']<=12)) {
            return false;
        }

        if($data['dayWeek'] != '*' && !($data['dayWeek']>=1 && $data['dayWeek']<=7)) {
            return false;
        }

        if(empty($data['workerClass'])) {
            return false;
        }

        return $data;
    }

}
