<?php

namespace ScheduledTask;

class ScheduledTask
{
    /** @var int|string contains minute, range: 0-59 or '*' - every */
    private $minute;

    /** @var int|string contains hour, range: 0-23 or '*' - every */
    private $hour;

    /** @var int|string contains days, range: 1-31 or '*' - every */
    private $day;

    /** @var int|string contains month, range: 1-12 or '*' - every */
    private $month;

    /** @var int|string contains day of week, range: 1-7  or '*' - every*/
    private $dayWeek;

    /** @var string contains worker class, \Workers\BaseWorker */
    private $workerClass;


    public function setMinute($minute = '*')
    {
        $this->minute = $minute;
        return $this;
    }

    public function setHour($hour = '*')
    {
        $this->hour = $hour;
        return $this;
    }

    public function setDay($day = '*')
    {
        $this->day = $day;
        return $this;
    }

    public function setMonth($month = '*')
    {
        $this->month = $month;
        return $this;
    }

    public function setDayWeek($dayWeek = '*')
    {
        $this->dayWeek = $dayWeek;
        return $this;
    }

    public function setWorkerClass($workerClass)
    {
        $this->workerClass = $workerClass;
        return $this;
    }

    public function getWorkerClass()
    {
        return $this->workerClass;
    }

    /**
     * Determines, whether the function should be execute at the given timestamp.
     *
     * @param int timestamp
     * @return boolean;
     */
    public function isRunByTime($timestamp)
    {
        if($this->checkCondition('dayWeek', 'N', $timestamp)) {
            if($this->checkCondition('month', 'n', $timestamp)) {
                if($this->checkCondition('day', 'j', $timestamp)) {
                    if($this->checkCondition('hour', 'G', $timestamp)) {
                        if($this->checkCondition('minute', 'i', $timestamp)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }


    /**
     * Checks @var in this object for compliance with the conditions at the specified timestamp.
     *
     * @param string param this object
     * @param string date format for function date
     * @param int timestamp
     * @return boolean;
     */
    private function checkCondition(string $param, string $dateFormat, int $timestamp): bool
    {
        return $this->{$param} == '*' || (int) $this->{$param} === (int) date($dateFormat, $timestamp);
    }
}
