<?php

class FileSystem
{
    public static function checkOrCreateDir($dir)
    {
        if(file_exists($dir) && !is_dir($dir)) {
            unlink($dir);
        }

        if(!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return true;
    }

    public static function checkFile($filePath): string
    {
        return realpath($filePath);
    }

    public static function newFileCreate(string $filePath)
    {
        if(file_exists($filePath)) {
            $count = count(glob($filePath.'.*'));
            rename($filePath, sprintf('%s.%s',$filePath,$count));
        }
        file_put_contents($filePath,'');
        return $filePath;
    }
}
