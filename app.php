<?php

require_once("config.php");

spl_autoload_register(function ($class_name) {
    $class_name = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);
    require_once sprintf('%s/App/%s.php',WORK_DIR,$class_name);
});


FileSystem::checkOrCreateDir(dirname(PATH_LOG_OUT));
FileSystem::checkOrCreateDir(dirname(PATH_LOG_ERR));

FileSystem::newFileCreate(PATH_LOG_OUT);
FileSystem::newFileCreate(PATH_LOG_ERR);


function redirect_StreamIO()
{
    global $STDIN, $STDOUT, $STDERR;

    ini_set('error_log', PATH_LOG_ERR);

    fclose(STDIN);
    fclose(STDOUT);
    fclose(STDERR);

    $STDIN = fopen('/dev/null', 'r');
    $STDOUT = fopen(PATH_LOG_OUT, 'ab');
    $STDERR = fopen(PATH_LOG_ERR, 'ab');
}
